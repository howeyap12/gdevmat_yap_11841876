Circle[] matters = new Circle[200];
int timer = 0;
void setup()
{
  size(1920, 1080, P3D);
  camera(0,0, Window.eyeZ, 0,0,0,0,-1,0);
  background(0);
  spawnMatter(matters, spawnBlackhole());
  noStroke();
}

void draw()
{
  for(int i = 0; i < matters.length; i++)
  {
    matters[i].display();
    matters[i].sucked();
  }
  timer++;
  if  (timer > 500)
  {
    clear();
    timer = 0;
    spawnMatter(matters, spawnBlackhole());
  }
}

void spawnMatter(Circle[] matters, PVector holePos)
{
    for(int i = 0; i < matters.length; i++)
    {
      float mean = 0;
      float standardDeviation = 200;
      float x = (standardDeviation * randomGaussian()) + mean;
      float mean2 = 0;
      float standardDeviation2 = 200;
      float y = (standardDeviation2 * randomGaussian()) + mean2;
      color c = color(random(255),random(255),random(255));
      matters[i] = new Circle(x,y,c,random(20), holePos);
      print(x,y);
    }
}

PVector spawnBlackhole()
{
  float myX = random(Window.left+250, Window.right-250);
  float myY = random(Window.bottom+250, Window.top-250);
  fill(255);
  circle(myX, myY, 50);
  return new PVector(myX, myY);
}
