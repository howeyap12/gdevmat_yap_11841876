class Circle
{
  PVector myPos;
  float size;
  color col;
  PVector holePos;
  
  Circle(float x, float y, color c, float s, PVector hPos)
  {
    myPos.x = x;
    myPos.y = y;
    col = c;
    size = s;
    holePos = hPos;
  }
  
  void display() 
  { 
    fill(col); 
    circle(myPos.x, myPos.y, size);
  }
  
  void sucked()
  {
    PVector direction = PVector.sub(holePos, myPos);
    myPos.add(direction);
  }
}
