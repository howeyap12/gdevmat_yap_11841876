ArrayList<Mover> movers;

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  //movers += new Mover(1);
  //mover.position.x = Window.left + 50;
  movers = new ArrayList<Mover>();
  for (int i = 0; i < 10; i++)
  {
    Mover newMover = new Mover(i+1);
    newMover.setColor(random(255), random(255), random(255));
    newMover.position.x = Window.left +50;
    movers.add(newMover);
  }
  
  noStroke();
}

PVector wind = new PVector(.1f, 0);
PVector gravity = new PVector(0, -1);

void draw()
{
  background(255);
  
  for (int i = 0; i < 10; i++)
  {
    Mover mover = movers.get(i);
    mover.render();
    mover.update();
    mover.applyForce(wind);
    mover.applyForce(gravity);
    
    if (mover.position.x > Window.right)
    {
      mover.velocity.x *= -1;
      mover.position.x = Window.right;
    }
    if (mover.position.y < Window.bottom)
    {
      mover.velocity.y *= -1;
      mover.position.y = Window.bottom;
    }
    
  }
  
}
