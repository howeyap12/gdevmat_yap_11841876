public class Mover
{
   public PVector position = new PVector();
   public PVector velocity = new PVector();
   public PVector acceleration = new PVector();
   
   public float r = 255, g = 255, b = 255, a = 255;
   public float mass;
   
   Mover(float ass)
   {
     this.mass = ass;
   }
   
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, this.mass * 10); 
   }
   
   public void setColor(float r, float g, float b)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = 200;
   }
   
   public void update()
   {
      // we add the acceleration to the velocity every frame
      this.velocity.add(this.acceleration);
      
      this.velocity.limit(30);
     
      //we add the velocity to the position every frame
      this.position.add(this.velocity);
      
      this.acceleration.mult(0);
   }
   
   public void applyForce(PVector force)
   {
     PVector f = PVector.div(force, this.mass);
     this.acceleration.add(f);
   }
}
