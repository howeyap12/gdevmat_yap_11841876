void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, -(height/2.0) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, -1, 0);
  background(0);
  noStroke();
}

Walker walker = new Walker();

void draw()
{
  walker.perlinWalk();
}
