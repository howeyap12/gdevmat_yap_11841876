class Walker
{
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  
  float xMove = 50;
  float yMove = 150;
  float pSize = 5;
  
  Walker()
  {
  
  };

  void perlinWalk()
  {
    r_t += 0.1f;
    g_t += 0.1f;
    b_t += 0.1f;
    xMove += 0.01f;
    yMove += 0.01f;
    pSize += 0.01f;
    
    fill((map(noise(r_t),0,1,0,255)), (map(noise(g_t),0,1,0,255)), (map(noise(b_t),0,1,0,255)), 255);
    circle(map(noise(xMove),0,1,-500,500), map(noise(yMove),0,1,-500,500), map(noise(pSize),0,1,10,120));
  }

}
