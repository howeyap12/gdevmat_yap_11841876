void setup()
{
  size(1920, 1000, P3D);
  camera(0, 0, (height/2) / tan(PI*30.0 / 180.0), 0, 0, 0, 0, 1, 0);
  background(0);
}

int timer = 0;
void draw()
{
  timer++;
  if  (timer > 1000)
  {
    clear();
    timer = 0;
  }
  float mean = 0;
  float standardDeviation = 200;
  float x = (standardDeviation * randomGaussian()) + mean;
  
  fill(random(255),random(255),random(255));
  circle(x,random(-500, 500),random(100));
}
