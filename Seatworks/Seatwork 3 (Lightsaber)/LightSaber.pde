float index = 150;
float index2 = 50;

void setup()
{
  size(1920, 1080, P3D);
  camera(0,0, Window.eyeZ, 0,0,0,0,-1,0);
}


void draw()
{
  index += 0.01;
  index2 += 0.01;
  background(0);
  PVector perlin = perlinMove();
  

  strokeWeight(15);
  perlin.normalize();
  perlin.mult(200);
  
  stroke(map(noise(index),0,1,0,255),map(noise(index2),0,1,0,255),50);
  line(0,0,perlin.x,perlin.y);  
  
  stroke(map(noise(index),0,1,0,255),map(noise(index2),0,1,0,255),50);
  line(0,0,-perlin.x,-perlin.y);  
  
  stroke(220,220,220);
  line(-perlin.x/10,-perlin.y/10,perlin.x/10,perlin.y/10);
  
  //foreground
  strokeWeight(4);
  stroke(255,255,255);  
  line(0,0,perlin.x,perlin.y);
  stroke(255,255,255);  
  line(0,0,-perlin.x,-perlin.y);
  
  stroke(100,100,100);
  line(-perlin.x/12,-perlin.y/12,perlin.x/12,perlin.y/12);
}

PVector perlinMove()
{
  float px = map(noise(index),0,1,-200,200);
  float py = map(noise(index2),0,1,-200,200);
  return new PVector(px,py);
}
