Mover mover;

void setup()
{
  size(1920, 1080, P3D);
  camera(0,0,Window.eyeZ,0,0,0,0,-1,0);
  
  mover = new Mover();
  mover.position.x = Window.left + 50;
  mover.acceleration = new PVector(0.1,0);
}

void draw()
{
  background(255);
  
  fill(200, 0,0);
  mover.render();
  
  if(mover.position.x > Window.right/2 + 20)
  {
    mover.position.x = Window.left+50;
  }
  
  if(mover.position.x > Window.windowWidth/20)
  {
    mover.acceleration.x = -0.5;
  }
  
  if(mover.velocity.x < 2)
  {
    mover.acceleration.x = 0.1;
  }
}
