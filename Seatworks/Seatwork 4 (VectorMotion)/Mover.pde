class Mover
{
  PVector position = new PVector();
  PVector velocity = new PVector();
  PVector acceleration = new PVector();
  
  float scale = 50;
  
  void render()
  {
    update();
    circle(position.x, position.y, scale);
  }
  
  void update()
  {
    this.velocity.add(this.acceleration);
    this.position.add(this.velocity);
  }
}
