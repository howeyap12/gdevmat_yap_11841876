Mover[] movers = new Mover[5];

void setup()
{
  size(1080, 720, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  
  for (int i = 0; i < 5; i++)
  {
    movers[i] = new Mover(Window.left + 50, -280 + (i*140));
    movers[i].mass = random(10);
    movers[i].setColor(random(1, 255),random(1, 255),random(1, 255), random(150, 255));
  }
}

PVector wind = new PVector(0.1f, 0);

void draw()
{
  background(255);
  
  noStroke();
  for (Mover m : movers)
  {
    m.render();
    m.update();
    
    if (m.position.x < Window.right/2 -300)
    {
      m.applyForce(wind);
    }
    else if (m.velocity.x > .1)
    {
      m.applyFriction();
    }
    else if (m.velocity.x < .1)
    {
      PVector stop = m.velocity;
      stop.mult(-1);
      m.acceleration.add(stop);
      m.acceleration.x = 0;
    }
  }
  

}
